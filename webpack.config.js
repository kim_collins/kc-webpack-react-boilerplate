const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

// extract to a single css file
const extractPlugin = new ExtractTextPlugin({
    filename: './css/app.css',
});

const config = {
    context: path.resolve(__dirname, 'src'),
    entry: [
        './js/app.js',
        './scss/app.scss',
    ],
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: './js/bundle.js',
    },
    resolve: { // resolve extensions - can add aliases too
        extensions: ['.js', '.jsx', '.json', '.scss', '.css'],
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            presets: ['env'],
                        },
                    },
                    {
                        loader: 'eslint-loader',
                    },
                ],
            },
            {
                test: /\.html$/,
                use: ['html-loader'],
            },
            // sass-loader with sourceMap activated
            {
                test: /\.scss$/,
                include: [path.resolve(__dirname, 'src', 'scss')],
                use: extractPlugin.extract({
                    use: [
                        {
                            loader: 'css-loader',
                            options: {
                                sourceMap: true,
                            },
                        },
                        {
                            loader: 'sass-loader',
                            options: {
                                sourceMap: true,
                            },
                        },
                    ],
                    fallback: 'style-loader',
                }),
            },
        ],
    },
    plugins: [
        new CleanWebpackPlugin(['dist']),
        new HtmlWebpackPlugin({
            template: 'index.html',
        }),
        extractPlugin,
    ],
    devServer: {
        contentBase: path.resolve(__dirname, 'dist/media'),
        stats: 'errors-only',
        open: true,
        port: 12000,
        compress: true,
    },
    devtool: 'inline-source-map',
};

module.exports = config;
