# KC Webpack3 React Boilerplate

Basic webpack setup to support react, es6, scss. CSS and JS bundling to single files.

## Getting Started

Just run npm install.

### Prerequisites

Requires node ~v.6, npm ~v.3.10


### Installing

Run ```npm run build``` to build. Dist folder should be created with index.html, js/bundle.js, and css/app.css

Run ```npm run dev``` to launch site on localhost:12000

### Environment Notes

If using VSCode, install the EditorConfig extension

## Authors

**Kim Collins**


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Thanks for references from https://github.com/postNirjhor/webpack-boilerplate, https://www.valentinog.com/blog/react-webpack-babel/

