import React from 'react';
import Child from './Child.js';

class Parent extends React.PureComponent {
    constructor() {
        super();

        this.state = {
            childText: 'original text',
            changed: false,
        };
    }
    handleClick() {
        if (this.state.changed === true) {
            this.setState({
                childText: 'original text',
                changed: false,
            });
        } else {
            this.setState({
                childText: 'you changed the text',
                changed: true,
            });
        }
    }
    render() {
        return (
            <Child text={this.state.childText} handleClick={this.handleClick.bind(this)} />
        );
    }
}

export default Parent;
