import React from 'react';
import PropTypes from 'prop-types';

const Child = ({ text, handleClick }) => {
    return (
        <div>
            <h2>Child component.</h2>
            <p>{text}</p>
            <button type="button" onClick={handleClick}>Change Text</button>
        </div>
    );
};

Child.propTypes = {
    text: PropTypes.string.isRequired,
    handleClick: PropTypes.func.isRequired,
};

export default Child;
