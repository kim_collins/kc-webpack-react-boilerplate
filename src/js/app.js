import React from 'react';
import ReactDOM from 'react-dom';
import Parent from './components/Parent.js';

const rootEl = document.getElementById('parent-root');
if (rootEl) {
    ReactDOM.render(
        <Parent />,
        rootEl
    );
}
